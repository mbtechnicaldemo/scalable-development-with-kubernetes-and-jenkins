## Scalable Development with Jenkins on Kubernetes

This repository is an instructional resource for **manually** configuring and deploying your own Jenkins automation suite into a GCP Kubernetes cluster.

From one developer to another, I am not going to waste our time on the specific ins and outs of Kubernetes or Jenkins.
I assume you already have a basic to intermediate knowledge of web service, Jenkins, Kubernetes, and Google Cloud Platform.

This document is a work in progress.
---

#Table of Contents#

####SECTION 1: Google Cloud Specific
	- Creating a new project
	- Google Cloud and Kubernetes
	- intro to pods, workloads, and services
	- Deploy a Kubernetes cluster
			
####SECTION 2: Jenkins Deployment
	- Deploy Jenkins on your GCP Kubernetes cluster
	- Domain setup to your load balancer (I'm using Google Cloud DNS)
	- Configure the HTTP(s) load balancer to serve secure content with an SSL certificate

####SECTION 3: Jenkins Configuration
	- Administrative setup
	- Jenkins Plugins
	- Ocean Blue Ocean

---

#Google Cloud Specific

For Kubernetes resources and how to get started, please access this [documentation page](https://cloud.google.com/kubernetes-engine/docs/).

If you don't know how, read up on [how to create a new project.](https://cloud.google.com/resource-manager/docs/creating-managing-projects)

Now, go ahead and create a new project.

![alt text](https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/dc4d2b2b772e5a1c9e3801f790376d7d7b89e2cf/files/NewProject2.PNG "New Project")

With our new project selected, navigate with the triple bar menu to Compute > Kubernetes Engine > Clusters.
	- If this is your first time on this page, it will take a while to prepare Kubernetes for you.
	
![alt text](https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/dc4d2b2b772e5a1c9e3801f790376d7d7b89e2cf/files/Navigate2Kubernetes.PNG "Find Kubernetes Clusters")

We will now create a new cluster by clicking the "Create Cluster" button.

Navigate through the prompts and make changes if you like. I am using the default configuration with no changes for now.

![alt text](https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/dc4d2b2b772e5a1c9e3801f790376d7d7b89e2cf/files/KubernetesLaunchCluster.PNG "Default Cluster")

You should now have a default kubernetes cluster with no special deployments - this should be a clean slate for the next Section.

---

#Jenkins Deployment

Now that we both have a Kubernetes cluster deployed, lets go ahead and put Google's Marketplace to good use.

Access the google Marketplace.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/ba27ed572da028f7f85abe21a2b12caa955a124d/files/Marketplace.PNG "Wow! A Marketplace for all things Kubernetes!")

Select Jenkins from the list!

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/ba27ed572da028f7f85abe21a2b12caa955a124d/files/Jenkins.PNG "New App")

Now go ahead and Configure.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/ba27ed572da028f7f85abe21a2b12caa955a124d/files/JenkinsConfig.PNG "Configure")

Your screen should look pretty similar to this. I'm not going to go into depth on the configuration details at this time. You can click deploy now.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/ba27ed572da028f7f85abe21a2b12caa955a124d/files/DeployJenkins.PNG "Deploy")

When you see this screen, it's time to go make yourself a coffee or something. Return in 5-10 minutes.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/ba27ed572da028f7f85abe21a2b12caa955a124d/files/cup-of-coffee.PNG "Coffee Time!")

Your deployment is complete when you click on Services and see something like this.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/6162650cbc6618d46bbda998b1b3ef998df3b277/files/ServicesFinished.PNG "Complete!")

##Serve Jenkins Securely SSL & Domain Setup on Google Cloud

Because we want to access jenkins securely across the internet, we need to setup an SSL cert provisioned by GCP. We will need a domain record in a DNS for this.
I won't get into the details of DNS setup, but here is the basics.

###Domain Setup

Navigate to Network Services > Cloud DNS and create a new Zone.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/6162650cbc6618d46bbda998b1b3ef998df3b277/files/CreateDNS.PNG "New Zone")

If needed, review the documentation provided by your domain registrar on how to set NameServer records to the following.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/6162650cbc6618d46bbda998b1b3ef998df3b277/files/ns-data.PNG "NS Records")

Once that is done and good, it may take some time for the DNS record changes to populate through the internet.

###SSL Setup

Navigate to Network Services > Load Balancing and find our load balancer created by the Jenkins deployment. We are going edit the HTTPS records.
Access your load balancer and inspect the front end items before proceeding. You should see a line for HTTPS listing an auto-generated certificate. 
Go ahead and click edit > frontend configuration and click the pencil next to the one starting with Protocol:HTTPS.

You should see something like this:

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/7c9d75523d83ada6055912a5a65020459241a86b/files/SSL-cert.PNG "Frontend Editor")

Click the SSL Dropdown and create a new certificate using your chosen domain or subdomain name before clicking Done. Click Update.

Navigate back to your Cloud DNS Zone previously configured and create a new A record pointing to the IP listed under the Load Balancer Frontend.

GCP will take a while to provision the cert, but you should be able to access Jenkins for configuration in the meantime at your domain.



---

#Jenkins Configuration

If you navigate to your domain you should now see the Jenkins Getting Started dialogue.

We will need to get the starting admin password from the server logs. This will require us to access the Kubernetes cluster shell. Return to the Kubernetes Clusters command panel and click connect.

Now, for the simplicity of this instructional, we will just access the Google Cloud Shell.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/0d33d80562bb6ab56b319ed0d95ecab2760e94f7/files/Connect%20to%20cluster%20-%2011.PNG "Cloud Shell")

When you see the Shell arrive, select the line by clicking anywhere in the black and press enter to execute the pre-filled command. You will want to run the following command:

	- kubectl get pods
	
This will list your deployment info. Copy the result of the jenkins-jenkins-deployment and run kubectl logs [deployment] - for me it looks like this:

	-kubectl logs jenkins-jenkins-deployment-767b6bd964-hd9zv

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/0d33d80562bb6ab56b319ed0d95ecab2760e94f7/files/Shell%20-%2013.PNG "Final Logs")

You should now look through the output for the following - your first admin password.

![alt text]( https://bitbucket.org/mbtechnicaldemo/scalable-deployment-pipeline/raw/0d33d80562bb6ab56b319ed0d95ecab2760e94f7/files/shell%20-%2014.PNG "Admin Logs")

You will use this to finish the basic configuration for Jenkins. The rest of basic configuration I will leave up to you.

---

#OCEAN BLUE OCEAN

You have probably heard of the Blue Ocean Jenkins community project. If you haven't I recommend looking up it's value to pipeline design. Essentially it is a visual editor for Jenkins Pipeline Management.
This can be extremely useful for those new to Jenkins, or without proper time to study and learn full Jenkinsfile manual configuration.


